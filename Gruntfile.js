module.exports = function(grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        uglify: {
            options: {
                banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n',
            },

            build: {
                files: {
                    'public/frontend.min.js': ['src/views/**/*.js', 'src/models/**/*.js', 'src/collections/**/*.js'],
                    'server.min.js': ['src/index.js']
                }
            }
        }
    });
    grunt.loadNpmTasks('grunt-contrib-uglify');
};