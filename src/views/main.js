$(function () {
    var MainView = Backbone.View.extend({
        el: 'body',
        initialize: function() {
            var that = this;
            $.get('views/main.mst', function(template) {
                that.template = template;
                that.render();
            });
        },  
        render: function() {
            this.$el.html(Mustache.render(this.template, { message: 'Hi' }));
            return this;
        },  
    });

    var Main = new MainView;
});