var express = require('express');
var app = express();
var mustache = require('mustache-express');
app.engine('mst', mustache());
app.set('view engine', 'mst');
app.set('views', __dirname + '/public/views/');
app.use(express.static('public'));
app.get('/', function(req, res) {
    res.render('index');
});
app.listen(3000, function() {

});